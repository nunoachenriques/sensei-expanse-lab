import matplotlib.pyplot as plt
import pandas as pd
import streamlit as st
from sensai.expanse import Expanse
from sensai.analysis import Analysis

st.title("SensAI Expanse Lab")

st.header("Connect")

db_name = st.text_input(label="Database name", value="sensei-expanse-lab")
db_user = st.text_input(label="Database user", value="expanse")
db_password = st.text_input(label="Database user password", value=None, type="password")
db_host = st.text_input(label="Database host", value="localhost")
db_port = st.number_input(label="Database port", value=5432)
# st.write(f"{db_name} | {db_user} | {db_password} | {db_host} | {db_port}")
expanse = Expanse()
if not expanse.connect(db_name, db_user, db_password, db_host, db_port):
    st.warning("FAILED to connect! HINT: Check your data? Is your database running?")
    st.stop()

st.header("Study")

st.subheader("Data analysis")

with st.spinner("Data analysis in progress..."):
    analysis = Analysis(
        demographics=pd.DataFrame(data=expanse.entity_demographics()),
        moments=pd.DataFrame(data=expanse.entity_data_moments()),
        collected=pd.DataFrame(data=expanse.collected("app.human.smile", "polarity")),
        gender_name=expanse.gender_names(language="eng"),
        valence_colour=expanse.valence_colours()
    )
# figure = plt.figure(num="dichotomic", figsize=(10, 5), tight_layout=True)
figure = plt.figure(num="dichotomic", tight_layout=True)
data = analysis.plt_valence_by_age_gender(
    df=(analysis.data_report_count
        .drop(columns="age_range_bin")
        .rename(columns={"age_range_dic_bin": "age_range_bin"})),
    figure=figure,
    dichotomic=True
)
st.pyplot(fig=figure)

st.subheader("Correlation towards happiness")

entity = st.selectbox("Select entity", analysis.eligible["entity"])
# st.write(expanse.entity_demographics())
