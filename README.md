[![Apache 2.0 License](https://img.shields.io/badge/license-Apache%202.0-blue.svg "Apache 2.0 License")][1]
[![security: bandit](https://img.shields.io/badge/security-bandit-yellow.svg)](https://github.com/PyCQA/bandit)
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/nunoachenriques/sensei-expanse-lab)](https://gitlab.com/nunoachenriques/sensei-expanse-lab)

# SensAI Expanse Lab --- Experiences regarding SensAI Expanse

[**SensAI Expanse**](https://gitlab.com/nunoachenriques/sensei-expanse) is
the expansion of [**SensAI**](https://gitlab.com/nunoachenriques/sensei) app
resources. This is the laboratory for ad hoc experiences.

## Develop

https://gitlab.com/nunoachenriques/sensei-expanse-lab

### Fork

In order to facilitate pull requests if you want to contribute then go to
https://gitlab.com/nunoachenriques/sensei-expanse-lab and hit **fork**!

### Install

```bash
git clone git@gitlab.com:{YOUR_OWN_USER}/sensei-expanse-lab.git
```

In order to keep your system's Python untainted, `sensei-expanse-lab` is
contained in a virtual environment. Therefore, install `pipenv` for the
environment management.

```bash
apt install python3-dev libpq-dev
sudo pip install pipenv
```

Inside the `sensei-expanse-lab` project directory, install dependencies creating
the environment. Moreover, install `pre-commit` into the project's `.git` hooks
--- it will keep you safe from usual code style or security mistakes when
committing changes.

```bash
pipenv install --dev
pipenv run pre-commit install
```

### Check

A `sensei-expanse` database with data is required such as `sensei-database-lab`
with a proper user to access the data (e.g., `expanse`).

### Run

A Web browser is required to interact with the presentation layer using
[Streamlit](https://streamlit.io).

```bash
pipenv run streamlit run app.py
```

## License

Copyright 2021 Nuno A. C. Henriques [http://nunoachenriques.net/]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[1]: http://www.apache.org/licenses/LICENSE-2.0.html
